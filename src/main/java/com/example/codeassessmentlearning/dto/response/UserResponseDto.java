package com.example.codeassessmentlearning.dto.response;


import com.example.codeassessmentlearning.dto.request.UserRequestDto;
import com.example.codeassessmentlearning.entity.User;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserResponseDto {

    private Long id;

    private String name;

    private String phone;

    private String email;

    private String username;

    private String password;

    private String role;

    public UserResponseDto() {
    }

    public UserResponseDto(UserRequestDto userRequestDto) {
        this.id = userRequestDto.getId();
        this.name = userRequestDto.getName();
        this.email = userRequestDto.getEmail();
        this.phone = userRequestDto.getPhone();
        this.role = userRequestDto.getRole();
        this.password = userRequestDto.getPassword();
        this.username = userRequestDto.getUsername();
    }


    public UserResponseDto(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.role = user.getRole();
        this.password = user.getPassword();
        this.username = user.getUsername();
    }

    public UserResponseDto(Long id, String name, String phone, String email, String username, String password, String role) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.username = username;
        this.password = password;
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRole() {
        return this.role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
