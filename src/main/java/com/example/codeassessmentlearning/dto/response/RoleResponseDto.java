package com.example.codeassessmentlearning.dto.response;

import com.example.codeassessmentlearning.entity.Role;

public class RoleResponseDto {
    private Long id;
    private String name;

    public RoleResponseDto(Role role) {
        this.id = role.getId();
        this.name = role.getName();
    }

    public RoleResponseDto() {
    }

    public RoleResponseDto(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
