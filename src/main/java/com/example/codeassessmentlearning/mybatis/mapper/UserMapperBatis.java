package com.example.codeassessmentlearning.mybatis.mapper;

import com.example.codeassessmentlearning.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapperBatis {

    List<User> selectUsers();

    void insertUser(User user);

    User selectUserById(Long id);

    User selectUserByUsername(String username);

    void deleteUser(Long id);

    void updateUser(User user);

}
