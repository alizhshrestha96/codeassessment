package com.example.codeassessmentlearning.exception;

public class UserDoesNotExistsException extends RuntimeException {

    private String message;

    public UserDoesNotExistsException() {

    }

    public UserDoesNotExistsException(String msg) {
        super(msg);
        this.message = msg;
    }

}
