package com.example.codeassessmentlearning.enums;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.NUMBER_INT)
public enum ResponseStatus {

    FAILURE,
    SUCCESS

}
