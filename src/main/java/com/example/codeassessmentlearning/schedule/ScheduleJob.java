package com.example.codeassessmentlearning.schedule;

import com.example.codeassessmentlearning.dto.request.UserRequestDto;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduleJob {

    private final JavaMailSender javaMailSender;

    private UserRequestDto userRequestDto;

    public ScheduleJob(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    @Scheduled(cron = "*/50 * * * * *")
    public void job() {

        System.out.println("Sending Email...");

        try {
            sendEmail();

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println("Done");
    }

    public void setUserRequestDto(UserRequestDto userRequestDto) {
        this.userRequestDto = userRequestDto;
    }

    void sendEmail() {

        SimpleMailMessage msg = new SimpleMailMessage();

        if (userRequestDto != null) {
            msg.setTo("musemateproduction@gmail.com");
            msg.setSubject("Welcome" + this.userRequestDto.getName());
            msg.setText(this.userRequestDto.toString());
        } else {
            msg.setTo("musemateproduction@gmail.com");
            msg.setSubject("Welcome");
            msg.setText("This is the test..........");
        }

        javaMailSender.send(msg);


    }
}
