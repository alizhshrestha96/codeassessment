package com.example.codeassessmentlearning.service;

import com.example.codeassessmentlearning.dto.request.UserRequestDto;
import com.example.codeassessmentlearning.dto.response.UserResponseDto;

import java.util.List;

public interface UserService {

    UserResponseDto saveUser(UserRequestDto userRequestDto);

    List<UserResponseDto> getUsers();

    UserResponseDto getUser(Long id);

    void deleteUser(Long user_id);
}
