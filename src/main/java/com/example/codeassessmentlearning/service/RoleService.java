package com.example.codeassessmentlearning.service;

import com.example.codeassessmentlearning.dto.request.RoleRequestDto;
import com.example.codeassessmentlearning.dto.response.RoleResponseDto;

public interface RoleService {
    RoleResponseDto saveRole(RoleRequestDto roleRequestDto);
}
