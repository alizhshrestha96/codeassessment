package com.example.codeassessmentlearning.service.impl;

import com.example.codeassessmentlearning.dto.request.UserRequestDto;
import com.example.codeassessmentlearning.dto.response.UserResponseDto;
import com.example.codeassessmentlearning.entity.User;
import com.example.codeassessmentlearning.exception.UserNotFoundException;
import com.example.codeassessmentlearning.mybatis.mapper.UserMapperBatis;
import com.example.codeassessmentlearning.schedule.ScheduleJob;
import com.example.codeassessmentlearning.service.UserService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final ScheduleJob scheduleJob;

    private final UserMapperBatis userMapperBatis;

    private final BCryptPasswordEncoder passwordEncoder;

    public UserServiceImpl(ScheduleJob scheduleJob,
                           UserMapperBatis userMapperBatis,
                           BCryptPasswordEncoder passwordEncoder) {
        this.scheduleJob = scheduleJob;
        this.userMapperBatis = userMapperBatis;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public UserResponseDto saveUser(UserRequestDto userRequestDto) {
        User user = new User(userRequestDto);
        user.setPassword(passwordEncoder.encode(userRequestDto.getPassword()));

        User getUser = userMapperBatis.selectUserById(userRequestDto.getId());

        if (getUser != null) {
            userMapperBatis.updateUser(user);
        } else {
            userMapperBatis.insertUser(user);
        }

        UserResponseDto userResponseDto = new UserResponseDto(userRequestDto);

        if (userResponseDto.getEmail() != null
                && !userResponseDto.getEmail().isEmpty()) {
            scheduleJob.setUserRequestDto(userRequestDto);
            scheduleJob.job();
        }


        return userResponseDto;
    }

    @Override
    public List<UserResponseDto> getUsers() {
        List<User> users = userMapperBatis.selectUsers();

        List<UserResponseDto> userResponseDtos = users.stream().map(u -> {
            UserResponseDto userResponse = new UserResponseDto(u);
            return userResponse;
        }).collect(Collectors.toList());

        return userResponseDtos;
    }

    @Override
    public UserResponseDto getUser(Long id) {

        User user = userMapperBatis.selectUserById(id);

        if (user != null) {
            UserResponseDto userResponseDto = new UserResponseDto(user);
            return userResponseDto;
        } else {
            throw new UserNotFoundException("Customer not found with id: " + id);
        }
    }


    @Override
    public void deleteUser(Long user_id) {
        userMapperBatis.deleteUser(user_id);
        System.out.println("User deleted successfully");

    }


}
