package com.example.codeassessmentlearning.service.impl;

import com.example.codeassessmentlearning.dto.request.RoleRequestDto;
import com.example.codeassessmentlearning.dto.response.RoleResponseDto;
import com.example.codeassessmentlearning.entity.Role;
import com.example.codeassessmentlearning.service.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    @Override
    public RoleResponseDto saveRole(RoleRequestDto roleRequestDto) {
        Role role = new Role(roleRequestDto);

        RoleResponseDto roleResponseDto = new RoleResponseDto(role);

        return roleResponseDto;
    }
}
