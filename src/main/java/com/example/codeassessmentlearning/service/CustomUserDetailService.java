package com.example.codeassessmentlearning.service;

import com.example.codeassessmentlearning.dto.CustomUserDetail;
import com.example.codeassessmentlearning.entity.User;
import com.example.codeassessmentlearning.exception.UserNotFoundException;
import com.example.codeassessmentlearning.mybatis.mapper.UserMapperBatis;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class CustomUserDetailService implements UserDetailsService {

    private final UserMapperBatis userMapperBatis;

    public CustomUserDetailService(UserMapperBatis userMapperBatis/*, UserMapper userMapper*/) {
        this.userMapperBatis = userMapperBatis;
    }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userMapperBatis.selectUserByUsername(username);

        if (user == null) {
            throw new UserNotFoundException("No User");
        }

        return new CustomUserDetail(user);
    }
}
