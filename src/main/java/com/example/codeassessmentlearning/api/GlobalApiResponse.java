package com.example.codeassessmentlearning.api;

import com.example.codeassessmentlearning.enums.ResponseStatus;

import java.io.Serializable;


public class GlobalApiResponse implements Serializable {

    private ResponseStatus status;
    private String message;
    private Object data;

    public void setResponse(String message, ResponseStatus status, Object data) {
        this.message = message;
        this.status = status;
        this.data = data;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
