package com.example.codeassessmentlearning;

import com.example.codeassessmentlearning.mybatis.mapper.UserMapperBatis;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class CodeAssessmentLearningApplication {

    private UserMapperBatis userMapperBatis;

    public static void main(String[] args) {
        SpringApplication.run(CodeAssessmentLearningApplication.class, args);
    }

}
