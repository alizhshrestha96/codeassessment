package com.example.codeassessmentlearning.controller.base;

import com.example.codeassessmentlearning.api.GlobalApiResponse;
import com.example.codeassessmentlearning.enums.ResponseStatus;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BaseController {

    protected final ResponseStatus API_SUCCESS_STATUS = ResponseStatus.SUCCESS;
    protected final ResponseStatus API_ERROR_STATUS = ResponseStatus.FAILURE;
    public ObjectMapper objectMapper = new ObjectMapper();
    /**
     * Module Name
     */
    protected String permissionName;
    protected String permissionName2;

    /**
     * This module is used to fetch the available permissions of current user in particular module
     */
    protected String module;
    protected String module2;

    protected GlobalApiResponse getSuccessResponse(String message, Object data) {
        GlobalApiResponse globalApiResponse = new GlobalApiResponse();
        globalApiResponse.setStatus(API_SUCCESS_STATUS);
        globalApiResponse.setMessage(message);
        globalApiResponse.setData(data);
        return globalApiResponse;

    }


    protected GlobalApiResponse errorResponse(String message, Object errors) {
        GlobalApiResponse globalApiResponse = new GlobalApiResponse();
        globalApiResponse.setStatus(API_ERROR_STATUS);
        globalApiResponse.setMessage(message);
        globalApiResponse.setData(errors);
        return globalApiResponse;
    }
}
