package com.example.codeassessmentlearning.controller;

import com.example.codeassessmentlearning.api.GlobalApiResponse;
import com.example.codeassessmentlearning.controller.base.BaseController;
import com.example.codeassessmentlearning.dto.request.RoleRequestDto;
import com.example.codeassessmentlearning.service.RoleService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/roles")
public class RoleController extends BaseController {

    private final RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    @PostMapping
    public ResponseEntity<GlobalApiResponse> saveRole(@RequestBody RoleRequestDto roleRequestDto) {
        return ResponseEntity.ok(getSuccessResponse("Role created successfully", roleService.saveRole(roleRequestDto)));
    }
}
