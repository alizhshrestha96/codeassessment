package com.example.codeassessmentlearning.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/public")
public class HomeController {

    @GetMapping("/home")
    public String welcome() {
        return "Welcome!!";
    }

    @GetMapping("/login")
    public String login() {
        return "You have logged in!!";
    }
}
