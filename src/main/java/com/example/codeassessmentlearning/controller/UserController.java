package com.example.codeassessmentlearning.controller;

import com.example.codeassessmentlearning.api.GlobalApiResponse;
import com.example.codeassessmentlearning.controller.base.BaseController;
import com.example.codeassessmentlearning.dto.request.UserRequestDto;
import com.example.codeassessmentlearning.service.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/users")
public class UserController extends BaseController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping
    public ResponseEntity<GlobalApiResponse> saveUser(@RequestBody @Valid UserRequestDto userRequestDto) {
        String message;
        if (userRequestDto.getId() == null) {
            message = "saved";
        } else {
            message = "updated";
        }

        return ResponseEntity.ok(getSuccessResponse("User " + message + " successfully", userService.saveUser(userRequestDto)));
    }


    @GetMapping
    public ResponseEntity<GlobalApiResponse> getUsers() {
        return ResponseEntity.ok(getSuccessResponse("retrieved successfully", userService.getUsers()));
    }


    @GetMapping("/{id}")
    public ResponseEntity<GlobalApiResponse> getUser(@PathVariable("id") Long id) {
        return ResponseEntity.ok(getSuccessResponse("retrieved successfully", userService.getUser(id)));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteUser(@PathVariable("id") Long id) {
        userService.deleteUser(id);
        System.out.println("User deleted successfully with user id: " + id);
        return ResponseEntity.ok("Deleted Successfully");
    }


}
