package com.example.codeassessmentlearning.entity;


import com.example.codeassessmentlearning.dto.request.RoleRequestDto;

import javax.persistence.*;

@Entity
public class Role {

    @Id
    @SequenceGenerator(name = "role_sequence", sequenceName = "role_sequence", allocationSize = 1)
    @GeneratedValue(generator = "role_sequence", strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;

    public Role(RoleRequestDto roleRequestDto) {
        this.id = roleRequestDto.getId();
        this.name = roleRequestDto.getName();
    }

    public Role() {
    }

    public Role(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
